#!/bin/bash
# Installation and setup of Gitea on Rocky-Linux devices
# Jdev - 14/11/2021

# 1. Installation of Wget and download of Gitea
dnf install wget -y
if [[ ! -e gitea-1.15.6-linux-amd64.asc ]]; then
wget -O gitea https://dl.gitea.io/gitea/1.15.6/gitea-1.15.6-linux-amd64
chmod +x gitea
fi

# 2. Verify signature
wget https://dl.gitea.io/gitea/1.15.6/gitea-1.15.6-linux-amd64.asc
echo " "
echo "============= <!> SIGNATURE VERIFICATION <!> ============="
gpg --keyserver keys.openpgp.org --recv 7C9E68152594688862D62AF62D9AE806EC1592E2
gpg --verify gitea-1.15.6-linux-amd64.asc gitea
while true; do
    read -r -p "Do you want to continue the installation ? [y/n]" yn
    case $yn in
        [Yy]* )
            break;;
        [Nn]* )
            exit
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done

# 3. Install git
dnf install -y git

# 4. Create user to run gitea
adduser \
  --system \
  --shell /bin/bash \
  --comment 'Git Version Control' \
  --create-home \
  --home /home/git \
  git

# 5. Create required directory structure
mkdir -p /var/lib/gitea/custom
mkdir -p /var/lib/gitea/data
mkdir -p /var/lib/gitea/log
chown -R git:git /var/lib/gitea
chmod -R 750 /var/lib/gitea
rm -rf /etc/gitea
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea

# 6. Copy gitea to the working directory
cp gitea /usr/local/bin/gitea

# 7. Run gitea as Linux service
if [[ ! -e /etc/systemd/system/gitea.service ]]; then
touch /etc/systemd/system/gitea.service
fi
cat > /etc/systemd/system/gitea.service << EOF
[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
###
# Don't forget to add the database service dependencies
###
#
Wants=mysql.service
After=mysql.service
#
#Wants=mariadb.service
#After=mariadb.service
#
#Wants=postgresql.service
#After=postgresql.service
#
#Wants=memcached.service
#After=memcached.service
#
#Wants=redis.service
#After=redis.service
#
###
# If using socket activation for main http/s
###
#
#After=gitea.main.socket
#Requires=gitea.main.socket
#
###
# (You can also provide gitea an http fallback and/or ssh socket too)
#
# An example of /etc/systemd/system/gitea.main.socket
###
##
## [Unit]
## Description=Gitea Web Socket
## PartOf=gitea.service
##
## [Socket]
## Service=gitea.service
## ListenStream=<some_port>
## NoDelay=true
##
## [Install]
## WantedBy=sockets.target
##
###

[Service]
# Modify these two values and uncomment them if you have
# repos with lots of files and get an HTTP error 500 because
# of that
###
#LimitMEMLOCK=infinity
#LimitNOFILE=65535
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
# If using Unix socket: tells systemd to create the /run/gitea folder, which will contain the gitea.sock file
# (manually creating /run/gitea doesn't work, because it would not persist across reboots)
#RuntimeDirectory=gitea
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea
# If you install Git to directory prefix other than default PATH (which happens
# for example if you install other versions of Git side-to-side with
# distribution version), uncomment below line and add that prefix to PATH
# Don't forget to place git-lfs binary on the PATH below if you want to enable
# Git LFS support
#Environment=PATH=/path/to/git/bin:/bin:/sbin:/usr/bin:/usr/sbin
# If you want to bind Gitea to a port below 1024, uncomment
# the two values below, or use socket activation to pass Gitea its ports as above
###
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_BIND_SERVICE
###

[Install]
WantedBy=multi-user.target
EOF

# 8. Enable and start Gitea at boot
systemctl enable gitea
systemctl start gitea

# 9. Adding working port on firewall
firewall-cmd --add-port=3000/tcp --permanent
firewall-cmd --reload

# 10. Installation of Mysql client
dnf install @mysql
systemctl enable mysqld
systemctl start mysqld

# 11. Make mysql listen to your ip address
mkdir /etc/mysql/
if [[ ! -e /etc/mysql/my.cnf ]]; then
touch /etc/mysql/my.cnf
fi
echo "Please enter the IP of the database device!"
read database_adress
cat > /etc/mysql/my.cnf << EOF
[mysqld]
bind-address = ${database_adress}
EOF

# 12. NFS Client setup
dnf -y install nfs-utils
firewall-cmd --add-service=nfs --permanent
firewall-cmd --reload
echo "Please enter the NFS Server IP!"
read nfsip
echo "Please enter the NFS Server Hostname!"
read nfs_hostname
echo "Please enter your Domain name!"
read domainName
echo "${nfsip} ${nfs_hostname}.${domainName}" >> /etc/hosts
echo "Please enter the path of the NFS share directory (on the server device)!"
read nfsPath
sed -i "s/#Domain.*/Domain = ${domainName}/" /etc/idmapd.conf
mkdir -p /backups
mount -t nfs ${nfs_hostname}.${domainName}:${nfsPath} /backups
echo "List of partition"
df -hT
echo "${nfs_hostname}.${domainName}:${nfsPath} /backups               nfs     defaults        0 0" >> /etc/fstab

echo " "
echo "You can now go to 'your.ip.device:3000' on a browser to finish the installation !"