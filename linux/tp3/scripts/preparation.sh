#!/bin/bash
# Preparation of Rocky-Linux devices
# Jdev - 14/11/2021

# 0. Values setup 
type_ip = $1
device_hostname = $2
device_dns = $3

if [[ !$1 ]]; then
    echo "Please enter the type of IP you want! [dhcp/static]"
    read type_ip
fi
if [[ ${type_ip} == "static" ]]; then
    echo "Please enter the IP that you want!"
    read device_ip
    echo "Please enter the netmask!"
    read device_netmask
fi
if [[ !$2 ]]; then
    echo "Please enter the hostname that you want for your device!"
    read device_hostname
fi
if [[ !$3 ]]; then
    echo "Please enter the DNS you want to use!"
    read device_dns
fi

# 1. DHCP IP Config
if [[ ${type_ip} == "dhcp" ]]; then
cat > /etc/sysconfig/network-scripts/ifcfg-enp0s8 << EOF
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=dhcp
ONBOOT=yes
EOF
# 1-1. Static IP Config
else
cat > /etc/sysconfig/network-scripts/ifcfg-enp0s8 << EOF
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=${device_ip}
NETMASK=${device_netmask}
EOF
fi
# 1-2. Reload for apply
nmcli con reload
nmcli con up enp0s8

# 2. DNS
cat > /etc/sysconfig/network-scripts/ifcfg-enp0s8 << EOF
nameserver ${device_dns}
EOF

# 3. Change hostname
hostname ${device_hostname}
cat > /etc/hostname << EOF
${device_hostname}
EOF

# 4. Firewall
systemctl enable firewalld
systemctl start firewalld
firewall-cmd --permanent --remove-service=cockpit
firewall-cmd --permanent --remove-service=dhcpv6-client
firewall-cmd --reload

# 5. DNF update ?
while true; do
    read -r -p "Do you want to do a 'dnf update' ? [y/n]" yn
    case $yn in
        [Yy]* )
            dnf update
            break;;
        [Nn]* )
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
