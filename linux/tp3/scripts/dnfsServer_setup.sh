#!/bin/bash
# Setup of the DNFS server
# Jdev - 14/11/2021

dnf -y install nfs-utils

echo "Please enter your device domain name!"
read domainName
sed -i "s/#Domain.*/Domain = ${domainName}/" /etc/idmapd.conf

echo "Please enter the path of the NFS share directory!"
echo "(script will create it if does not exist)"
read nfsPath
echo "Please enter your network IP"
echo "Example : 10.0.0.0/24"
read networkIp
mkdir -p ${nfsPath}
cat > /etc/exports << EOF
# create new
# for example, set [/home/nfsshare] as NFS share
${nfsPath} ${networkIp}(rw,no_root_squash)
# 
EOF
systemctl enable --now rpcbind nfs-server

#Firewall
firewall-cmd --add-service=nfs --permanent
firewall-cmd --reload