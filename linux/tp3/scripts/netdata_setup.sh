#!/bin/bash
# Installation and setup Netdata on Rocky-Linux devices
# Jdev - 28/10/21

# 0. Value setup
DISCORDWEBHOOK=${1//\//\\\/}

ACTIONCHANNEL=$2

# 1. Netdata installation
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)

# 2. Add netdata workings ports to firewall-cmd
firewall-cmd --add-port=8125/tcp --permanent
firewall-cmd --add-port=19999/tcp --permanent
firewall-cmd --reload

# 3. Make a copy of the config file to write in (if it not already exit)
if [[ ! -f /opt/netdata/etc/netdata/health_alarm_notify.conf ]]; then
    cp -R -f "/opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf" "/opt/netdata/etc/netdata/health_alarm_notify.conf"
fi

# 4. Get Webhook and Channel if they were not already given then write in the conf file.
# Reading the value from input if not given at start :
if [[ -z $DISCORDWEBHOOK ]]; then
    echo "Enter your discord webhook :"
    read -r DISCORDWEBHOOK
fi
# Search and replace in the conf file with the value
echo Writing "$DISCORDWEBHOOK" to the configuration file.
sed -i "s/^DISCORD_WEBHOOK_URL=\".*\"$/DISCORD_WEBHOOK_URL=\"${DISCORDWEBHOOK//\//\\\/}\"/g" /opt/netdata/etc/netdata/health_alarm_notify.conf

# Reading the value from input if not given at start :
if [[ -z $ACTIONCHANNEL ]]; then
    echo "Enter the name of the channel where you want the notifications :"
    read -r ACTIONCHANNEL
fi
# Search and replace in the conf file with the value
echo Writing "$ACTIONCHANNEL" to the configuration file.
sed -i "s/^DEFAULT_RECIPIENT_DISCORD=\".*\"$/DEFAULT_RECIPIENT_DISCORD=\"${ACTIONCHANNEL//\//\\\/}\"/g" /opt/netdata/etc/netdata/health_alarm_notify.conf 

# 5. Restart Netdata service to apply modifications
systemctl restart netdata

# 6. Asking for a notification test
while true; do
    read -r -p "Do you want a test notification ? [y/n]" yn
    case $yn in
        [Yy]* )
            # export yo=toto ;sudo -u tor echo $yo
            export NETDATA_ALARM_NOTIFY_DEBUG=1
            sudo -u netdata /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
            break;;
        [Nn]* )
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
# YES or NO we do the following command :
sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf

# 7. End of script
echo "Installation completed !"