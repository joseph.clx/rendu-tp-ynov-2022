## Database Preparation

**We want to use one VM for one services for our installation, so we setup a remote database :**

## Script

And an other one !

You can found [my script](scripts/database_setup.sh) to install and create your database on a clean linux device.

## Installation details
### Step 1: Update Rocky Linux

As usual we run a global update first : 

```bash
sudo dnf udpate
```

### Step 2: Enable MySQL Upstream Module

Moving along, we need to enable the MySQL 8.0 module:

```bash
sudo dnf module list mysql
```

To enable the MySQL module stream, run the command:

```bash
sudo dnf module enable mysql:8.0
```

### Step 3: Install MySQL 8.0 on Rocky Linux

With the module enabled, install MySQL 8.0 in Rocky Linux as follows:

```bash
sudo dnf install @mysql
```

### Step 4: Enable and Start MySQL

```bash
sudo systemctl start mysqld; sudo systemctl enable mysqld
```

You can check if the service is started by using: 

```bash
sudo systemctl status mysqld
```

**Enable working port on firewall :**

```bash
sudo firewall-cmd --add-port=3306/tcp --permanent
success
sudo firewall-cmd --reload
success
```
### Step 5: Secure MySQL in Rocky Linux

The final step is to secure the just installed MySQL database server. We need to harden it by running the mysql_secure_installation script.

```bash
sudo mysql_secure_installation
```

Be sure to set a **strong MySQL root password** in accordance with the password level selected.

For the remaining prompts, **press ‘Y’** to remove anonymous users, prevent the root user from logging in remotely, and remove the test database which should be purged before proceeding into a production environment.

### Step 6: Create a new user:

Create database user which will be used by Gitea, authenticated by password. This example uses 'gitea' as password. Please use a secure password for your instance.

```sql
CREATE USER 'gitea'@'192.168.10.2' IDENTIFIED BY 'myPassword';
```

### Step 7: Create database with UTF-8 charset and collation:

Make sure to use utf8mb4 charset instead of utf8 as the former supports all Unicode characters (including emojis) beyond Basic Multilingual Plane. Also, collation chosen depending on your expected content. When in doubt, use either unicode_ci or general_ci.

```sql
CREATE DATABASE giteadb CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';
```

### Step 8: Grant all privileges on the database to database user created above:

```sql
GRANT ALL PRIVILEGES ON giteadb.* TO 'gitea'@'192.168.10.2';
FLUSH PRIVILEGES;
exit
```

### Step 10: Make MySQL listen to your IP address (on the gitea device).

```sql
sudo mkdir /etc/mysql/; sudo nano /etc/mysql/my.cnf
sudo cat /etc/mysql/my.cnf
[mysqld]
bind-address = 192.168.10.3
```

### Step 9: On your Gitea server, test connection to the database:

```bash
mysql -u gitea -h 192.168.10.3 -p giteadb
```

**You should be connected to the database.**

You are now ready to start the installation of Gitea : 
[Go check here](SERVER.md).