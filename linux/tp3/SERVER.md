# Set up the server device for my Git server (on Rocky-Linux).

## Gitea server

## Script 

And an other one !

You can found [my script](scripts/gitea_setup.sh) to install and create your gitea on a clean linux device.

## Installation details
### 1. Dependencies

As usual we run a global update first : `dnf udpate`

### 2. Download

We use wget, you can install it with `dnf install wget`

```bash
wget -O gitea https://dl.gitea.io/gitea/1.15.6/gitea-1.15.6-linux-amd64
chmod +x gitea
```

### 3. Verify GPG signature

Gitea signs all binaries with a GPG key to prevent against unwanted modification of binaries. To validate the binary. 

Download the signature file which ends in .asc for the binary you downloaded and use the gpg command line tool.

```bash
wget https://dl.gitea.io/gitea/1.15.6/gitea-1.15.6-linux-amd64.asc
```

Look for the text Good signature from "Teabot <teabot@gitea.io>" to assert a good binary, despite warnings like This key is not certified with a trusted signature!.

```bash
gpg --keyserver keys.openpgp.org --recv 7C9E68152594688862D62AF62D9AE806EC1592E2
gpg --verify gitea-1.15.6-linux-amd64.asc gitea
```

### 4. Prepare environment

#### Install Git

```bash
sudo dnf install -y git
```

#### Create user to run Gitea (ex. git)

```bash
sudo adduser \
   --system \
   --shell /bin/bash \
   --comment 'Git Version Control' \
   --create-home \
   --home /home/git \
   git
```

#### Create required directory structure

```bash
sudo mkdir -p /var/lib/gitea/custom
sudo mkdir -p /var/lib/gitea/data
sudo mkdir -p /var/lib/gitea/log
sudo chown -R git:git /var/lib/gitea
sudo chmod -R 750 /var/lib/gitea
sudo rm -rf /etc/gitea
sudo mkdir /etc/gitea
sudo chown root:git /etc/gitea
sudo chmod 770 /etc/gitea
```

#### Copy gitea to the working directory 

```bash
sudo cp gitea /usr/local/bin/gitea
```

**NOTE:** `/etc/gitea` is temporary set with write rights for user git so that Web installer could write configuration file. After installation is done, it is recommended to set rights to read-only using

```bash
chmod 750 /etc/gitea
chmod 640 /etc/gitea/app.ini
```

#### Run Gitea as Linux service

**Using systemd:**

Copy the sample [gitea.service](https://github.com/go-gitea/gitea/blob/main/contrib/systemd/gitea.service) to `/etc/systemd/system/gitea.service`, then edit the file with your favorite editor.

Uncomment any service that needs to be enabled on this host, such as MySQL.

Change the user, home directory, and other required startup values.

**Enable and start Gitea at boot:**

```bash
sudo systemctl enable gitea; sudo systemctl start gitea
```

**Adding working port on firewall:**

```bash
sudo firewall-cmd --add-port=3000/tcp --permanent
success
sudo firewall-cmd --reload
success
```

You can now go to `192.168.10.2:3000` on a browser to finish the installation !
