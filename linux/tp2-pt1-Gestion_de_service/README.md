﻿# Gestion de servie
## I. Un premier serveur web
### 1. Installation
🌞 **Installer le serveur Apache**

**paquet `httpd`**
```bash
sudo dnf -y install httpd
```

🌞 **Démarrer le service Apache**

```bash
[jdev@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:httpd.service(8)

[jdev@web ~]$ sudo systemctl start httpd
[jdev@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 16:31:52 CEST; 7s ago
     Docs: man:httpd.service(8)
 Main PID: 1519 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 4946)
   Memory: 25.1M
   CGroup: /system.slice/httpd.service
           ├─1519 /usr/sbin/httpd -DFOREGROUND
           ├─1520 /usr/sbin/httpd -DFOREGROUND
           ├─1521 /usr/sbin/httpd -DFOREGROUND
           ├─1522 /usr/sbin/httpd -DFOREGROUND
           └─1523 /usr/sbin/httpd -DFOREGROUND

Sep 29 16:31:52 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Sep 29 16:31:52 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Sep 29 16:31:52 web.tp2.linux httpd[1519]: Server configured, listening on: port 80

[jdev@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
Firewall
```bash
[jdev@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[jdev@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
SS : 
```bash
[jdev@web ~]$ sudo ss -alnpt
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process
[...]
LISTEN          0               128                                  *:80                                *:*             users:(("httpd",pid=1523,fd=4),("httpd",pid=1522,fd=4),("httpd",pid=1521,fd=4),("httpd",pid=1519,fd=4))
[...]
```

🌞 **TEST**

```bash
[jdev@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 16:31:52 CEST; 6min ago
     Docs: man:httpd.service(8)
 Main PID: 1519 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4946)
   Memory: 25.1M
   CGroup: /system.slice/httpd.service
           ├─1519 /usr/sbin/httpd -DFOREGROUND
           ├─1520 /usr/sbin/httpd -DFOREGROUND
           ├─1521 /usr/sbin/httpd -DFOREGROUND
           ├─1522 /usr/sbin/httpd -DFOREGROUND
           └─1523 /usr/sbin/httpd -DFOREGROUND

Sep 29 16:31:52 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Sep 29 16:31:52 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Sep 29 16:31:52 web.tp2.linux httpd[1519]: Server configured, listening on: port 80
```

```bash
[jdev@web ~]$ sudo systemctl is-enabled httpd
enabled
```

```bash
[jdev@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
	[...]
```
Curl depuis mon Pc : 
```bash
PS C:\Users\josep> curl 10.102.1.2
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
```

### 2. Avancer vers la maitrise du service

🌞 **Le service Apache...**

-   donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume : `sudo systemctl enable httpd`
- prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume : 
```bash
[jdev@web ~]$ sudo systemctl is-enabled httpd
enabled
```
- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```bash
[jdev@web ~]$ sudo find /etc -name httpd.service
/etc/systemd/system/multi-user.target.wants/httpd.service
```
```bash
[jdev@web ~]$ sudo cat /etc/systemd/system/multi-user.target.wants/httpd.service
[...]
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf qui définit quel user est utilisé : `User apache`

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf :
```bash
[jdev@web ~]$ ps -ef | grep httpd
root        1519       1  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1520    1519  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1521    1519  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1522    1519  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1523    1519  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
jdev        1901    1403  0 17:07 pts/0    00:00:00 grep --color=auto httpd
```
- vérifiez avec un `ls -al` le dossier du site (dans `/var/www/...`) : `ls -al /var/www/html` :
```bash
[jdev@web ~]$ ls -al /var/www/html
total 0
drwxr-xr-x. 2 root root  6 Jun 11 17:35 .
drwxr-xr-x. 4 root root 33 Sep 29 16:14 ..
```
Le contenu n'appartient pas à l'utilisateur apache

🌞 **Changer l'utilisateur utilisé par Apache**

_Info de l'user existant : `apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin`_

- Création de l'user :
`sudo useradd apacheAdmin --home /usr/share/httpd --shell /sbin/nologin`

- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur :

```bash
[jdev@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[jdev@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
[...]
User apacheAdmin
Group apacheAdmin
[...]
```

- redémarrez Apache et utilisez une commande `ps` pour vérifier que le changement a pris effet

```bash
[jdev@web ~]$ sudo systemctl restart httpd
[jdev@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 17:59:50 CEST; 13s ago
[...]
[jdev@web ~]$ ps -ef | grep httpd
root        2170       1  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheA+    2172    2170  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheA+    2173    2170  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheA+    2174    2170  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheA+    2175    2170  0 17:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
jdev        2409    1403  0 18:00 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port : 
```bash
[jdev@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[jdev@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 90
[...]
```
-   ouvrez un nouveau port firewall, et fermez l'ancien

```bash
[jdev@web ~]$ sudo firewall-cmd --add-port=90/tcp --permanent
success
[jdev@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[jdev@web ~]$ sudo firewall-cmd --reload
success
[jdev@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 90/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[jdev@web ~]$
```
-   redémarrez Apache et prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```bash
[jdev@web ~]$ sudo systemctl restart httpd
[jdev@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 18:09:54 CEST; 6s ago
[...]
[jdev@web ~]$ sudo ss -alnpt
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
[...]
LISTEN     0          128                        *:90                      *:*        users:(("httpd",pid=2495,fd=4),("httpd",pid=2494,fd=4),("httpd",pid=2493,fd=4),("httpd",pid=2490,fd=4))
```
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```bash
[jdev@web ~]$ curl localhost:90
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port
```bash
PS C:\Users\josep> curl 10.102.1.2:90
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
```

📁 **Fichier <a href="assets/httpd_I.conf">/etc/httpd/conf/httpd.conf</a>**
## II. Une stack web plus avancée
### 1. Intro
### 2. Setup
Les VM sont ready.
#### A. Serveur Web et NextCloud
🌞 **Install du serveur Web et de NextCloud sur `web.tp2.linux`**
- Uniquement pour le serveur Web + NextCloud, la base de données MariaDB après. (Quand ils parlent de la base de données, on saute juste l'étape, on le fait après).

**Installing And Configuring Repositories**

1. Installation de EPEL : `[jdev@web ~]$ sudo dnf install -y epel-release` puis mise à jour `sudo dnf update`. 

2. Installation de Remi repository run `sudo dnf install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm`.

3. Petite vérification rapide : 
```bash
[jdev@web ~]$ sudo dnf list installed | grep 'epel\|remi'
epel-release.noarch                  8-13.el8                              @extras
remi-release.noarch                  8.4-1.el8.remi                        @@commandline
```

4. Voir la liste des modules PHP qui peuvent être activés : `sudo dnf module list php`.

5. On prend la version la plus haute de PHP compatible avec NextCloud : `sudo dnf module enable php:remi-7.4`.

6. On vérifie que c'est bien activé (avec la présence du "[e]") :
```bash
[jdev@web ~]$ sudo dnf module list php
[...]
php                remi-7.4 [e]              common [d], devel, minimal              PHP scripting language
[...]
```

**Installing Packages**

On tape la grosse commande pour installer tout les Packages nécessaire (avec un -y pour pas perdre sa touche "Y"):

`sudo dnf install -y httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp`

**Configuring Packages And Directories**

1. On configure Apache pour démarrer au boot : `sudo systemctl enable httpd`

2. On créer les dossiers nécessaire :
   - Sites disponilbes : `sudo mkdir /etc/httpd/conf/sites-available`
   - Sites activés : `sudo mkdir /etc/httpd/conf/sites-enabled`
   - Repo des sites : `sudo mkdir /var/www/sub-domains/`

3. On active maintenant la lecture des fichiers dans le dossier `sites-enabled/` depuis le fichier de conf : 
```bash
[jdev@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[jdev@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
[...]
IncludeOptional /conf/sites-enabled/*
```

4. On créer la configuration avec : `sudo nano /etc/httpd/conf/sites-available/web.tp2.linux`
```bash
[jdev@web ~]$ sudo cat /etc/httpd/conf/sites-available/web.tp2.linux
#MyConf
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/web.tp2.linux/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

5. On créer un lien vers la configuration dans `/etc/httpd/conf/sites-enabled`  avec la commande `sudo ln -s /etc/httpd/conf/sites-available/web.tp2.linux /etc/httpd/conf/sites-enabled/`

**Creating The Directory**

1. Création du _DocumentRoot_ : `sudo mkdir -p /var/www/sub-domains/web.tp2.linux/html`. _(C'est ici qu'on va installer l'instance de NextCloud.)_

**Configuring PHP**

1. Je trouve ma timezone avec `timedatectl` :
```bash
[jdev@web ~]$ timedatectl | grep zone
                Time zone: Europe/Paris (CEST, +0200)
```

2. On modifie le fichier php.ini pour y ajouter notre Timezone :
```bash
[jdev@web ~]$ sudo nano /etc/opt/remi/php74/php.ini
[jdev@web ~]$ sudo cat /etc/opt/remi/php74/php.ini
[...]
[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
;date.timezone = "Europe/Paris"
[...]
```

3. Par bonne pratique, on vérifie que la time zone est bien la même que celle sur la machine :
```bash
[jdev@web ~]$ ls -al /etc/localtime
lrwxrwxrwx. 1 root root 34 Sep 15 15:30 /etc/localtime -> ../usr/share/zoneinfo/Europe/Paris
```

**Installing Nextcloud**

**Get The Nextcloud .zip File And Unzip**

1. Telechargement de NextCloud sur le serveur avec :
`wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip`

2. Puis Unzip du DL :
`unzip nextcloud-22.2.0.zip`

3. On va pas laisser tout ça ici, donc on copie ou il faut :
- `cd nextcloud`
- `cp -Rf * /var/www/sub-domains/web.tp2.linux/html/`

4. Attribuer les droits au bon user _(apache)_ :
`chown -Rf apache.apache /var/www/sub-domains/web.tp2.linux/html`

<u>Vérification</u>
```bash
[jdev@web ~]$ ls -al /var/www/sub-domains/web.tp2.linux/html/
total 120
drwxr-xr-x. 13 apache apache  4096 Oct  6 14:00 .
drwxr-xr-x.  3 root   root      18 Oct  6 11:25 ..
drwxr-xr-x. 43 apache apache  4096 Oct  6 14:00 3rdparty
drwxr-xr-x. 48 apache apache  4096 Oct  6 14:00 apps
-rw-r--r--.  1 apache apache 19327 Oct  6 14:00 AUTHORS
drwxr-xr-x.  2 apache apache    67 Oct  6 14:00 config
-rw-r--r--.  1 apache apache  3924 Oct  6 14:00 console.php
-rw-r--r--.  1 apache apache 34520 Oct  6 14:00 COPYING
drwxr-xr-x. 22 apache apache  4096 Oct  6 14:00 core
-rw-r--r--.  1 apache apache  5163 Oct  6 14:00 cron.php
-rw-r--r--.  1 apache apache   156 Oct  6 14:00 index.html
-rw-r--r--.  1 apache apache  3454 Oct  6 14:00 index.php
drwxr-xr-x.  6 apache apache   125 Oct  6 14:00 lib
-rw-r--r--.  1 apache apache   283 Oct  6 14:00 occ
drwxr-xr-x.  2 apache apache    23 Oct  6 14:00 ocm-provider
drwxr-xr-x.  2 apache apache    55 Oct  6 14:00 ocs
drwxr-xr-x.  2 apache apache    23 Oct  6 14:00 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  6 14:00 public.php
-rw-r--r--.  1 apache apache  5340 Oct  6 14:00 remote.php
drwxr-xr-x.  4 apache apache   133 Oct  6 14:00 resources
-rw-r--r--.  1 apache apache    26 Oct  6 14:00 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  6 14:00 status.php
drwxr-xr-x.  3 apache apache    35 Oct  6 14:00 themes
drwxr-xr-x.  2 apache apache    43 Oct  6 14:00 updater
-rw-r--r--.  1 apache apache   422 Oct  6 14:00 version.php
```

5. Par sécurité, on bouge le dossier `data` de DocumentRoot avec la commande : `mv /var/www/sub-domains/web.tp2.linux/html/data /var/www/sub-domains/web.tp2.linux/`

<!> Pas de dossier Data présent pour le moment
```bash
[jdev@web ~]$ mv /var/www/sub-domains/web.tp2.linux/html/data /var/www/sub-domains/web.tp2.linux/
mv: cannot stat '/var/www/sub-domains/web.tp2.linux/html/data': No such file or directory
```

6. On restart le tout pour appliquer les config : `systemctl restart httpd`
```bash
[jdev@web ~]$ sudo systemctl restart httpd
[jdev@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/httpd.service.d
           └─php74-php-fpm.conf
   Active: active (running) since Wed 2021-10-06 14:11:23 CEST; 4s ago
```

📁 **Fichier <a href="assets/httpd_II.conf">/etc/httpd/conf/httpd.conf</a>**  
📁 **Fichier <a href="assets/web.tp2.linux">/etc/httpd/conf/sites-available/web.tp2.linux</a>**
#### B. Base de données

**🌞 Install de MariaDB sur db.tp2.linux**

**Installing Packages**

Via la commande : `sudo dnf install -y mariadb-server`

**Configuring mariadb-server**

1. Setup de base 
```bash
[jdev@db ~]$ sudo systemctl enable mariadb
[...]
[jdev@db ~]$ sudo systemctl restart mariadb
```

2. On lance la commande `mysql_secure_installation` 

3. Vous repérerez le port utilisé par MariaDB avec une commande ss exécutée sur `db.tp2.linux` :

```bash
[jdev@db ~]$ sudo ss -alnpt
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
[...]
LISTEN     0          80                         *:3306                    *:*        users:(("mysqld",pid=4702,fd=21))
```

4. J'ouvre le port : 
```bash
[jdev@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[jdev@db ~]$ sudo firewall-cmd --reload
success
```

**🌞 Préparation de la base pour NextCloud**

- Une fois en place, il va falloir préparer une base de données pour NextCloud. Connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`

**🌞 Exploration de la base de données**

- Afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera.
```bash
[jdev@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```
- Trouver une commande qui permet de lister tous les utilisateurs de la base de données : `SELECT user FROM user;`
#### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

- On modifie le fichier host de notre PC pour lui ajouter `10.102.1.11 web.tp2.linux`.
- avec un navigateur, visitez NextCloud à l'URL http://web.tp2.linux
```bash
PS C:\Users\josep> curl web.tp2.linux                                                                                   

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>

RawContent        : HTTP/1.1 200 OK
                    Keep-Alive: timeout=5, max=100
                    Connection: Keep-Alive
                    Accept-Ranges: bytes
                    Content-Length: 156
                    Content-Type: text/html; charset=UTF-8
                    Date: Wed, 06 Oct 2021 13:52:37 GMT
                    ETag: "...
Forms             : {}
Headers           : {[Keep-Alive, timeout=5, max=100], [Connection, Keep-Alive], [Accept-Ranges, bytes],
                    [Content-Length, 156]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 156
```
- saisissez les informations pour que NextCloud puisse se connecter avec votre base
```bash
[jdev@web ~]$ sudo cat /var/www/sub-domains/web.tp2.linux/html/config/config.php | grep 'dbname\|dbhost\|dbuser\|dbpassw
ord'
  'dbname' => 'nextcloud',
  'dbhost' => '10.102.1.12:3306',
  'dbuser' => 'nextcloud',
  'dbpassword' => 'woof',
```

**🌞 Exploration de la base de données**

- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
```SQL
MariaDB [(none)]> SELECT count(*)
    -> FROM information_schema.tables
    -> WHERE table_schema = 'nextcloud';
+----------+
| count(*) |
+----------+
|      108 |
+----------+
1 row in set (0.001 sec)
```

<img src="assets/squidward-dab.gif" style="width: 300px;">

### Tableau Rendu
| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web (nextcloud)             | 80           |              |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données (mariadb)| 3306           | 10.102.1.11             |
