# TP3 : Progression vers le réseau d'infrastructure
## I. (mini) Architecture réseau
Réseaux :
```
#Réseau hôte #5
server1 : 10.3.1.0/25 (126 machines libres)
Masque : 255.255.255.128

#Réseau hôte #6 (62 machines libres)
client1 : 10.3.1.128/26
Masque : 255.255.255.192

#Réseau hôte #7 (14 machines libres)
server2 : 10.3.1.192/28
Masque : 255.255.255.240
```

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast] |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
|`client1`|`10.3.1.128`| `255.255.255.192` | 62                 |`10.3.1.130`         | `10.3.1.191`                                                                                   |
| `server1`     | `10.3.1.0`        | `255.255.255.128` | 126                           | `10.3.1.2`         | `10.3.1.127`                                                                                   |
| `server2`     | `10.3.1.192`        | `255.255.255.240` | 14                          | `10.3.1.194`         | `10.3.1.207`                                                                                   |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.1.130/25`         | `10.3.1.2/25`         | `10.3.1.194/28`         | Carte NAT             |
| `dhcp.client1.tp3`  | `10.3.1.131/25 ` | x | x | `10.3.1.130/25` |
| `marcel.client1.tp3`  | `Dynamique` | x | x | `10.3.1.130/25` |
| `dns1.server1.tp3`  | `10.3.1.3` | x | x | `10.3.1.2/25` |
### 2. Routeur

🖥️ **VM router.tp3**

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle
```
Mémo carte réseaux :
enp0s8 : Virtual Hôte #5
enp0s9 : Virtual Hôte #6
enp0s10 : Virtual Hôte #7
```

```bash
[jdev@router ~]$ ip a | grep enp0s
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.2/25 brd 10.3.1.127 scope global noprefixroute enp0s8
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.130/26 brd 10.3.1.191 scope global noprefixroute enp0s9
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.194/28 brd 10.3.1.207 scope global noprefixroute enp0s10
```

- il a un accès internet, il a de la résolution de noms :
```bash
[jdev@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=17.7 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 17.657/17.657/17.657/0.000 ms

[jdev@router ~]$ ping google.com
PING google.com (172.217.19.238) 56(84) bytes of data.
64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=1 ttl=114 time=17.2 ms
64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=2 ttl=114 time=17.5 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 17.249/17.373/17.497/0.124 ms
```
- il porte le nom `router.tp3` : 
```bash
[jdev@router ~]$ sudo nano /etc/hostname
[jdev@router ~]$ sudo cat /etc/hostname
router.tp3
[jdev@router ~]$ sudo hostname router.tp3
[jdev@router ~]$ hostname
router.tp3
```
- n'oubliez pas d'activer le routage sur la machine
```bash
[jdev@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
[jdev@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
[jdev@router ~]$ sudo firewall-cmd --reload
```

## II. Services d'infra
### 1. Serveur DHCP
🌞 **Mettre en place une machine qui fera office de serveur DHCP** dans le réseau `client1`. Elle devra :
- porter le nom `dhcp.client1.tp3`
```bash
[jdev@mother ~]$ sudo nano /etc/hostname
[jdev@mother ~]$ sudo cat /etc/hostname
dhcp.client1.tp3
[jdev@mother ~]$ sudo hostname dhcp.client1.tp3
[jdev@mother ~]$ hostname
dhcp.client1.tp3
```
- IP de la machine : 
```bash
[jdev@mother ~]$ ip a | grep enp0s
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.131/25 brd 10.3.1.255 scope global noprefixroute enp0s8
```

- Setup DHCP :
```bash
[jdev@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new
# specify domain name
option domain-name     "client1.tp3";
# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.128 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range 10.3.1.132 10.3.1.190;
    # specify broadcast address
    option broadcast-address 10.3.1.191;
    # specify gateway
    option routers 10.3.1.129;
}
```
- On active au boot et on vérifie que ca tourne
```bash
[jdev@dhcp ~]$ sudo systemctl enable dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
[jdev@dhcp ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
   Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-09-30 16:08:44 CEST; 1min 37s ago
```
- Firewall 
```bash
[jdev@dhcp ~]$ sudo firewall-cmd --add-service=dhcp
success
[jdev@dhcp ~]$ sudo firewall-cmd --runtime-to-permanent
success
[jdev@dhcp ~]$ sudo firewall-cmd --reload
success
```
- Indique l'adresse de leurs passerelle : 
```bash
[jdev@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[...]
    # specify gateway
    option routers 10.3.1.129;
}
```
```bash
[jdev@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[...]
# specify DNS server's hostname or IP address
option domain-name-servers 1.1.1.1;
[...]
}
```
📁 **Fichier <a href="assets/dhcpd.conf">dhcpd.conf</a>** 

🖥️ VM marcel.client1.tp3

🌞 Mettre en place un client dans le réseau client1
- la machine récupérera une IP dynamiquement grâce au serveur DHCP
```bash
[jdev@marcel ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
[jdev@marcel ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=dhcp
ONBOOT=yes

[jdev@marcel ~]$ sudo nmcli con reload
[jdev@marcel ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/4)

[jdev@marcel ~]$ ip a
[...]
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc 
[...]
    inet 10.3.1.132/26 brd 10.3.1.191 scope global dynamic 
[...]
```
- ainsi que sa passerelle et une adresse d'un DNS utilisable

🌞 Depuis marcel.client1.tp3
- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP
```bash
[jdev@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=16.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=16.3 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 16.323/17.548/19.452/1.364 ms
[jdev@marcel ~]$ ping google.com
PING google.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=113 time=17.4 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=113 time=17.0 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 17.015/17.195/17.375/0.180 ms
```
- à l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau
```bash
[jdev@marcel ~]$ traceroute google.com
traceroute to google.com (142.250.179.78), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.130)  1.210 ms  1.161 ms  1.180 ms
 2  10.0.2.2 (10.0.2.2)  1.051 ms  0.979 ms  0.948 ms
```
### 2. Serveur DNS
### A. Our own DNS server

🌞 Mettre en place une machine qui fera office de serveur DNS
- il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme google.com. (Conf classique avec le fichier /etc/resolv.conf ou les fichiers de conf d'interface).
```bash
[jdev@dns1 ~]$ sudo nano /etc/resolv.conf
[jdev@dns1 ~]$ sudo cat /etc/resolv.conf
# DNS Google
nameserver 8.8.4.4
```
- comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install du serveur DNS. Le paquet que vous allez installer devrait s'appeler bind : c'est le nom du serveur DNS le plus utilisé au monde

Je vérifie si Bind-utils est bien présent sur la VM.
```bash
[jdev@dns1 ~]$ dnf list installed | grep bind-utils
bind-utils.x86_64                    32:9.11.26-4.el8_4                  @appstream
```

On installe ce dont on as besoin :
`dnf -y install bind bind-utils` :

On modifie le fichier de config : 
```bash
[jdev@dns1 ~]$ sudo nano /etc/named.conf
```

```bash
[jdev@dns1 ~]$ sudo cat /etc/named.conf
[...]
acl "allowed" {
        10.3.1.0/25;
        10.3.1.128/26;
        10.3.1.192/28
};

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };

zone "server1.tp3" IN {
        type master;
        file "/var/named/server1.tp3.forward";
};
zone "server2.tp3" IN {
        type master;
        file "/var/named/server2.tp3.forward";
};        
[...]
```

**Fichier de zone Forward pour `server1.tp3`.**
- On copie `named.loopback` pour faire notre fichier de zone forward :
`[jdev@dns1 ~]$ sudo cp /var/named/named.loopback /var/named/server1.tp3.zone`

- On lui met les boins droits : 
```bash
[jdev@dns1 ~]$ sudo chmod 644 /var/named/server1.tp3.zone
[jdev@dns1 ~]$ sudo chown root:named /var/named/server1.tp3.zone
[jdev@dns1 ~]$ sudo ls -al /var/named/
[...]
-rw-r--r--.  1 root  named  168 Oct 10 09:37 server1.tp3.zone
[...]
```
- Puis on edit le fichier :
```bash
[jdev@dns1 ~]$ sudo vi /var/named/server1.tp3.zone
[jdev@dns1 ~]$ sudo cat /var/named/server1.tp3.zone
$TTL 86400
@   IN  SOA dns1.server1.tp3. (
        2021062301   ; serial
        3600         ; refresh
        1800         ; retry
        604800       ; expire
        86400 )      ; minimum TTL
;
; define nameservers
    IN  NS  dns1.server1.tp3.
;
; DNS Server IP addresses and hostnames
dns1 IN  A   10.3.1.3
;
;client records

EOL
```
**Fichier de zone Forward pour `server2.tp3`.**
- On refait les mêmes étapes
- La modification du fichier :
```bash
[jdev@dns1 ~]$ sudo vi /var/named/server2.tp3.zone
[jdev@dns1 ~]$ sudo cat /var/named/server2.tp3.zone
$TTL 86400
@   IN  SOA dns1.server2.tp3. (
        2021062301   ; serial
        3600         ; refresh
        1800         ; retry
        604800       ; expire
        86400 )      ; minimum TTL
;
; define nameservers
    IN  NS  dns1.server2.tp3.
;
; DNS Server IP addresses and hostnames
dns1 IN  A   10.3.1.3
;
;client records

EOL
```

### B. Setup copain
### 3. Get deeper
### A. DNS forwarder
### B. On revient sur la conf du DHCP
## Entracte
## III. Services métier
### 1. Serveur Web

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

`sudo dnf install -y nginx`
```bash
[jdev@web1 ~]$ sudo systemctl start nginx
[jdev@web1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-19 21:15:01 CEST; 4s ago
[...]
```

🌞 **Test test test et re-test**

testez que votre serveur web est accessible depuis marcel.client1.tp3

utilisez la commande curl pour effectuer des requêtes HTTP

```bash
[jdev@marcel ~]$ curl 10.3.1.195
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
[...]
```

### 2. Partage de fichiers
### A. L'introduction wola
### B. Le setup wola
## IV. Un peu théorie : TCP et UDP
## V. El final
