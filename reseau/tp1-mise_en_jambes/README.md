# I. Exploration locale en solo
## 1. Affichage d'informations sur la pile TCP/IP locale (Ubuntu)
### En ligne de commande
**Affichez les infos des cartes réseau de votre PC**
(Les commandes sont rentrée dans un terminal et les sudo sont présent pour des question de droit)
```sudo lshw -class network``` & ```ip a``` 

**Interface réseau sans fil :**
Nom : ```... : wlp3s0```
Adresse Mac : ```... : ac:ed:5c:48:c7:90```
IP : ```192.168.0.10```
**Interface ethernet :**
Nom : ```... : enp2s0```
Adresse Mac : ```... : 50:9a:4c:bf:1e:2c```
IP : Pas d'ip visible (surement car pas de câble branché)

**Affichez votre gateway**
```ip r | grep default```
Default Gateway : ```... 192.168.0.1 ...```
### En graphique 

<img src="assets/ipwifi_info.jpeg" alt="Les infos wifi GUI"> 

### A quoi sert la gateway dans le réseau d'YNOV ?

Le LAN d'YNOV utilise des protocoles différents de ceux d'Internet.
La gateway dans le réseau YNOV fonctionne comme un convertisseur de protocole pour que les utilisateurs puissent envoyer et recevoir à travers Internet.

## 2. Modifications des informations
### A. Modification d'adresse IP (part 1)
**Utilisez l'interface graphique de votre OS pour changer d'adresse IP :**

<img src="assets/change_ip.png" alt="Changement d'ip GUI">

**Il est possible que vous perdiez l'accès internet. Pourquoi ?**

Si l'adresse ip que je rentre est identique à l'adresse ip d'un autre appareil présent sur le réseau, cela posera des problèmes dans la réception d'informations.
### B. Table ARP
**Exploration de la table ARP**
```sudo arp -a``` nous donne :

```console
? (192.168.0.5) à f4:6b:ef:5a:92:25 [ether] sur wlp3s0
_gateway (192.168.0.1) à f4:6b:ef:5a:92:24 [ether] sur wlp3s0
```
L'adresse MAC de la passerelle de mon réseau est ```f4:6b:ef:5a:92:24```.
Elle est associée à l'adresse IP de la Gateway identifiée plus haut.

**Et si on remplissait un peu la table ?**
Ping vers des machines dans le réseau :
```ping 192.168.0.20```
```ping 192.168.0.35```
```ping 192.168.0.86```
```ping 192.168.0.155```

Ensuite ```sudo arp -a``` nous donne :
```console
? (192.168.0.5) à f4:6b:ef:5a:92:25 [ether] sur wlp3s0
? (192.168.0.86) à <incomplet> sur wlp3s0
? (192.168.0.20) à <incomplet> sur wlp3s0
? (192.168.0.35) à <incomplet> sur wlp3s0
? (192.168.0.155) à <incomplet> sur wlp3s0
_gateway (192.168.0.1) à f4:6b:ef:5a:92:24 [ether] sur wlp3s0
```
### C. Nmap

```sudo nmap -sn -PE 10.33.0.0/22``` pour scanner le réseau YNOV.

On reçoit :
```console
[...]
Nmap scan report for 10.33.3.206
Host is up (0.10s latency).
MAC Address: 78:31:C1:CF:CB:26 (Apple)
Nmap scan report for 10.33.3.219
Host is up (0.095s latency).
MAC Address: A0:78:17:B5:63:BB (Unknown)
Nmap scan report for 10.33.3.222
Host is up (0.026s latency).
MAC Address: 5C:BA:EF:0B:1D:81 (Unknown)
Nmap scan report for 10.33.3.225
Host is up (0.015s latency).
MAC Address: 34:2E:B7:49:5A:CF (Unknown)
Nmap scan report for 10.33.3.226
Host is up (0.0047s latency).
MAC Address: F0:77:C3:06:8F:42 (Unknown)
Nmap scan report for 10.33.3.229
Host is up (0.055s latency).
MAC Address: 94:08:53:46:75:D3 (Unknown)
Nmap scan report for 10.33.3.232
Host is up (0.014s latency).
MAC Address: 88:66:5A:4D:A7:F5 (Unknown)
Nmap scan report for 10.33.3.245
Host is up (0.15s latency).
MAC Address: EC:9B:F3:51:3B:1B (Samsung Electro-mechanics(thailand))
Nmap scan report for 10.33.3.252
Host is up (0.0021s latency).
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for _gateway (10.33.3.253)
Host is up (0.0044s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.069s latency).
[...]
```

Comme on peut le voir entre .219 et .222 je n'ai aucun retour, l'ip 10.33.3.220 semble donc libre.

La table ARP ```sudo arp -a``` :

```console
? (10.33.1.5) à <incomplet> sur wlp3s0
? (10.33.1.0) à <incomplet> sur wlp3s0
? (10.33.2.5) à <incomplet> sur wlp3s0
? (10.33.2.0) à <incomplet> sur wlp3s0
? (10.33.0.9) à <incomplet> sur wlp3s0
? (10.33.3.5) à <incomplet> sur wlp3s0
? (10.33.3.0) à <incomplet> sur wlp3s0
? (10.33.1.9) à <incomplet> sur wlp3s0
? (10.33.3.254) à 00:0e:c4:cd:74:f5 [ether] sur wlp3s0
? (10.33.0.4) à <incomplet> sur wlp3s0
? (10.33.2.9) à <incomplet> sur wlp3s0
_gateway (10.33.3.253) à 00:12:00:40:4c:bf [ether] sur wlp3s0
? (10.33.1.4) à <incomplet> sur wlp3s0
? (10.33.2.4) à <incomplet> sur wlp3s0
? (10.33.0.8) à ee:6b:a1:17:e3:b6 [ether] sur wlp3s0
? (10.33.3.4) à 74:d8:3e:3e:99:41 [ether] sur wlp3s0
? (10.33.3.3) à <incomplet> sur wlp3s0
? (10.33.1.8) à <incomplet> sur wlp3s0
? (10.33.2.193) à e0:2b:e9:7d:a6:c2 [ether] sur wlp3s0
? (10.33.2.8) à <incomplet> sur wlp3s0
? (10.33.0.2) à <incomplet> sur wlp3s0
? (10.33.1.7) à <incomplet> sur wlp3s0
? (10.33.3.8) à <incomplet> sur wlp3s0
? (10.33.0.1) à <incomplet> sur wlp3s0
? (10.33.1.2) à <incomplet> sur wlp3s0
? (10.33.2.7) à <incomplet> sur wlp3s0
? (10.33.1.1) à <incomplet> sur wlp3s0
? (10.33.2.2) à <incomplet> sur wlp3s0
? (10.33.3.7) à <incomplet> sur wlp3s0
? (10.33.1.93) à b8:9a:2a:3d:c1:1a [ether] sur wlp3s0
? (10.33.0.71) à f0:03:8c:35:fe:47 [ether] sur wlp3s0
? (10.33.2.1) à <incomplet> sur wlp3s0
? (10.33.2.234) à 28:df:eb:fb:50:7c [ether] sur wlp3s0
? (10.33.3.1) à <incomplet> sur wlp3s0
? (10.33.0.6) à <incomplet> sur wlp3s0
? (10.33.2.208) à a4:b1:c1:72:13:98 [ether] sur wlp3s0
? (10.33.0.5) à <incomplet> sur wlp3s0
? (10.33.3.179) à 94:e7:0b:0a:46:aa [ether] sur wlp3s0
```
### D. Modification d'adresse IP (part 2)

Comme précisé au dessus, l'IP **10.33.3.220** semble libre. Je modifie donc mon Ip sur celle ci.

<img src="assets/iplibre.png" alt="Setup de l'ip libre GUI">

Pour vérifier j'utilise un ```ip a``` :
```
[...]
inet 10.33.3.220/24
[...]
```
et un ```ip r | grep default``` pour la Gateway :
```default via 10.33.3.253 ...```

On re ping le réseau Ynov : ```sudo nmap -sn -PE 10.33.0.0/22```

On recoit : 
```console
[...]
MAC Address: 44:78:3E:82:8B:EE (Samsung Electronics)
Nmap scan report for 10.33.3.100
Host is up (3.9s latency).
MAC Address: 2A:84:55:56:76:EA (Unknown)
Nmap scan report for 10.33.3.112
Host is up (0.20s latency).
MAC Address: 3C:06:30:2D:48:0D (Unknown)
Nmap scan report for 10.33.3.125
Host is up (0.16s latency).
MAC Address: E0:D4:64:F4:25:7C (Unknown)
Nmap scan report for 10.33.3.131
Host is up (0.21s latency).
MAC Address: 3C:22:FB:84:D9:68 (Unknown)
Nmap scan report for 10.33.3.160
Host is up (0.19s latency).
MAC Address: A4:83:E7:71:58:57 (Unknown)
Nmap scan report for 10.33.3.161
Host is up (0.10s latency).
[...]
Nmap done: 1024 IP addresses (79 hosts up) scanned in 33.03 seconds
```

Pour la connexion internet ```ping 8.8.8.8``` nous donne :

```console
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=18.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=17.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=115 time=21.7 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=115 time=141 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3001ms
rtt min/avg/max/mdev = 17.555/49.918/141.974/53.172 ms
```

Je suis donc sur l'ip voulu, sur la Gateway d'YNOV et avec une connexion internet.

# II. Exploration locale en duo

Pour les prérequis je vérifie que mon Firewall est bien désactivé : 
```sudo ufw status``` -> ```État : inactif```

**Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :**

Modification de l'adresse IP :

<img src="assets/setiplocalexplo.png" alt="Changement d'ip GUI">

On vérifie avec ```ip a``` : 
```console
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 50:9a:4c:bf:1e:2c brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.1/30 brd 192.168.0.3 scope global noprefixroute enp2s0
```
puis on ping : 
```
PING 192.168.0.2 (192.168.0.2) 56(84) bytes of data.
64 bytes from 192.168.0.2: icmp_seq=1 ttl=64 time=0.559 ms
64 bytes from 192.168.0.2: icmp_seq=2 ttl=64 time=0.641 ms
64 bytes from 192.168.0.2: icmp_seq=3 ttl=64 time=0.630 ms
64 bytes from 192.168.0.2: icmp_seq=4 ttl=64 time=0.540 ms
^C
--- 192.168.0.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3057ms
rtt min/avg/max/mdev = 0.540/0.592/0.641/0.050 ms
```

Les deux machines sont biens connectées.

On regarde la table ARP ```sudo arp -a``` :
```console
_gateway (10.33.3.253) à 00:12:00:40:4c:bf [ether] sur wlp3s0
? (192.168.0.2) à 00:0e:c6:ee:e9:5e [ether] sur enp2s0
```

## 4. Utilisation d'un des deux comme gateway
### Vous allez désactiver Internet sur une des deux machines, et vous servir de l'autre machine pour accéder à internet.
#### désactivez l'interface WiFi sur l'un des deux postes
Sur le PC avec internet :
```console
ip a 
[...] 
2: enp0s31f6: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000 link/ether 54:e1:ad:d4:c4:62 brd ff:ff:ff:ff:ff:ff 3: wlp4s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noqueue state DOWN group default qlen 1000 link/ether f8:94:c2:2e:59:00 brd ff:ff:ff:ff:ff:ff 4: enx000ec6eee95e: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 link/ether 00:0e:c6:ee:e9:5e brd ff:ff:ff:ff:ff:ff inet 192.168.0.2/22 brd 192.168.3.255 scope global noprefixroute enx000ec6eee95e valid_lft forever preferred_lft forever inet6 fe80::bbb0:6332:936d:13e5/64 scope link noprefixroute valid_lft forever preferred_lft forever
```
#### s'assurer de la bonne connectivité entre les deux PCs à travers le câble RJ45
On refait un ping :
```console
PING 192.168.0.2 (192.168.0.2) 56(84) bytes of data.
64 bytes from 192.168.0.2: icmp_seq=1 ttl=64 time=0.693 ms
64 bytes from 192.168.0.2: icmp_seq=2 ttl=64 time=0.473 ms
64 bytes from 192.168.0.2: icmp_seq=3 ttl=64 time=0.791 ms
^C
--- 192.168.0.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2055ms
rtt min/avg/max/mdev = 0.473/0.652/0.791/0.134 ms
```
Après avoir setup le partage de connexion sur une nouvelle connexion filaire :

Traceroute avec internet :
```console
traceroute 1.1.1.1
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
 1  _gateway (10.33.3.253)  12.363 ms  15.285 ms  15.194 ms
 2  10.33.10.254 (10.33.10.254)  2.054 ms  3.274 ms  2.709 ms
 3  reverse.completel.net (92.103.174.137)  15.255 ms  15.214 ms  16.808 ms
 4  92.103.120.182 (92.103.120.182)  17.329 ms  16.723 ms  17.243 ms
 5  172.19.130.117 (172.19.130.117)  20.856 ms  21.119 ms  21.409 ms
 6  46.218.128.74 (46.218.128.74)  18.945 ms  24.344 ms  23.897 ms
 7  equinix-paris.cloudflare.com (195.42.144.143)  25.663 ms  34.394 ms  24.819 ms
 8  one.one.one.one (1.1.1.1)  16.283 ms  17.053 ms  16.587 ms
```

Traceroute du PC sans Internet 
```console
traceroute 1.1.1.1
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
 1  josephdev (192.168.0.1)  0.471 ms  0.432 ms  0.416 ms
 2  _gateway (10.33.3.253)  16.758 ms  16.977 ms  17.375 ms
 3  10.33.10.254 (10.33.10.254)  8.347 ms  8.331 ms  8.569 ms
 4  reverse.completel.net (92.103.174.137)  8.810 ms  8.795 ms  9.636 ms
 5  92.103.120.182 (92.103.120.182)  10.268 ms  10.546 ms  10.534 ms
 6  172.19.130.117 (172.19.130.117)  20.537 ms  17.016 ms  20.538 ms
 7  46.218.128.74 (46.218.128.74)  17.041 ms  17.121 ms  16.602 ms
 8  equinix-paris.cloudflare.com (195.42.144.143)  17.559 ms  17.033 ms  20.712 ms
 9  one.one.one.one (1.1.1.1)  18.764 ms  19.472 ms  18.659 ms
```

## 5. Petit chat privé
#### **sur le PC serveur**
```sudo nc -l -p 8888```
#### **sur le PC  client**
```sudo nc 192.168.0.2 8888```

On obtient 
```console
joseph : ~ [~~] -▸ sudo nc 192.168.0.2 8888
test1
test
PC Joseph : Bonjour
pc Adam : Bonjour
```

#### pour aller un peu plus loin

Sur le pc client 
```console
joseph : ~ [~~] -▸ sudo nc 192.168.0.2 9999
test
PC Joseph sur co privée
```

Sur le pc serveur :
```console
joseph : ~ [~~] -▸ sudo nc -p 9999 -s 192.168.0.2 -l 
test 
PC Joseph sur co privée
```

## 6. Firewall
#### Activez votre firewall
Pour activer le firewall : 
```sudo ufw enable```

Vérification : 

```console
joseph : ~ [~~] -▸ sudo ufw enable
...
Le pare-feu est actif et lancé au démarrage du système

joseph : ~ [~~] -▸ sudo ufw status
État : actif
```

#### Autoriser les `ping`

Sur Ubuntu les pings sont déjà autorisé de base, la preuve avec la commande
 `sudo nano /etc/ufw/before.rules `: 

```console
[...]
-A ufw-before-forward -p icmp --icmp-type echo-request -j ACCEPT
[...]
```

#### Autoriser le traffic sur le port qu'utilise `nc`

Pour autoriser le traffic sur le port 8888 :
`sudo ufw allow 8888`

```console
joseph : ~ [~~] -▸ sudo ufw allow 8888
La règle a été ajoutée
La règle a été ajoutée (v6)
```

Sur le pc serveur :

```console
jdev@jdev-sony-laptop:~$ sudo ufw status
Status : active

To                         Action      From
----                       ------      --
8888                       ALLOW       Anywhere                  
8888 (v6)                  ALLOW       Anywhere (v6) 
```
Puis on lance le nc : `sudo nc -l -p 8888`

On lance enfin sur le pc client : 
`sudo nc 192.168.0.2 8888`

```console
joseph : ~ [~~] -▸ sudo nc 192.168.0.2 8888
Pc Dell pour Pc Sony
Pc Sony pour Pc Dell
:)
```

# III. Manipulations d'autres outils/protocoles côté client
## 1. DHCP
### Exploration du DHCP, depuis votre PC
Je fais ces commandes depuis chez moi, le résultat sera donc surement différent.
#### afficher l'adresse IP du serveur DHCP du réseau :
`sudo dhclient -d -nw wlp3s0 ` 
->
```console
[...]
bound to 192.168.0.10 -- renewal in 37334 seconds.
[...]
```
Ici on peut voir l'ip attribuée et la durée avant le rafraîchissement. 

## 2. DNS

### trouver l'adresse IP du serveur DNS que connaît votre ordinateur

On utilise la commande : 
`sudo cat /etc/resolv.conf`
qui nous donne : 
```console
nameserver 127.0.0.53
options edns0
search numericable.fr
```
Ici l'adresse du DNS est 127.0.0.53.

### utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

On utilise donc le `dig` sur 
- **google.com** : 
```console
; <<>> DiG 9.11.3-1ubuntu1.15-Ubuntu <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 704
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		89	IN	A	216.58.206.238

;; Query time: 9 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Sun Sep 19 09:43:00 CEST 2021
;; MSG SIZE  rcvd: 55
```
- **ynov.com** :
```console
; <<>> DiG 9.11.3-1ubuntu1.15-Ubuntu <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62977
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;ynov.com.			IN	A

;; ANSWER SECTION:
ynov.com.		10800	IN	A	92.243.16.143

;; Query time: 49 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Sun Sep 19 09:45:10 CEST 2021
;; MSG SIZE  rcvd: 53
```
#### interpréter les résultats de ces commandes

Pour les deux commandes, on peut voir que je demande au DNS trouvé plus tôt (127.0.0.53) de récupérer l'adresse IP correspondante au nom de domaine **google.com** et **ynov.com**.

#### déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
```console
[...]
;; SERVER: 127.0.0.53#53(127.0.0.53)
[...]
```

#### faites un _reverse lookup_ (= "dis moi si tu connais un nom de domaine pour telle IP")
- Pour l'adresse 78.74.21.21 :
```console
joseph : ~ [~~] -▸ dig -x 78.74.21.21

; <<>> DiG 9.11.3-1ubuntu1.15-Ubuntu <<>> -x 78.74.21.21
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 59439
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;21.21.74.78.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
21.21.74.78.in-addr.arpa. 3572	IN	PTR	host-78-74-21-21.homerun.telia.com.

;; Query time: 0 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Sun Sep 19 09:52:53 CEST 2021
;; MSG SIZE  rcvd: 101
```
- pour l'adresse 92.146.54.88 :
```console
joseph : ~ [~~] -▸ dig -x 92.146.54.88

; <<>> DiG 9.11.3-1ubuntu1.15-Ubuntu <<>> -x 92.146.54.88
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 10456
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;88.54.146.92.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
88.54.146.92.in-addr.arpa. 3600	IN	PTR	apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr.

;; Query time: 47 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Sun Sep 19 09:54:19 CEST 2021
;; MSG SIZE  rcvd: 113
```

#### interpréter les résultats

On rajoute le -x pour le rDNS (Reverse DNS).
On demande au DNS (ici SFR) de nous renvoyer le nom de domaine associé à l'IP. Ce qu'on reçoit dans la partie **ANSWER SECTION**.

# IV. Wireshark

### utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
#### un `ping` entre vous et la passerelle

<img src="assets/wiresharkpinggateway.png" alt="Wireshark - lignes de pings">

#### un `netcat` entre vous et votre mate, branché en RJ45

<img src="assets/netcatwireshark.jpeg" alt="Wireshark - netcat communication">

#### une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.

<img src="assets/dnswireshark.png" alt="Wireshark - DNS wireshark">
